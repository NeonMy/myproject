<?php 

if (!defined('DOCUMENT_ROOT')) {
    exit('No direct script access allowed');
}    

class Router {
    
    private $full_path ='';
    private $array_path = array();
    
    
    public function __construct() {
        $this->parse_path();
    }
    
    private function parse_path() {
        $this->full_path = $_GET['path'];
        $this->array_path = explode('/', $_GET['path']);
        unset($_GET['path']);
        
        if (file_exists(DOCUMENT_ROOT . '/controller/'.$this->array_path[0] . '.php')) {
            require_once DOCUMENT_ROOT . '/controller/' . $this->array_path[0] . '.php';
            
            if (class_exists($this->array_path[0])) {
                $class = new $this->array_path[0]($this->array_path);
                $class->index();
            }
        }
    }
    
}