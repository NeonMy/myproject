<?php

if (!defined('DOCUMENT_ROOT')) {
    exit('No direct script access allowed');
}

/**
 * Контроллер обработки расчетов и логики
 */
class product {

    private $array_path = array();

    public function __construct($path) {
        $this->array_path = $path;
    }

    public function index() {
        $dataProduct = $this->connectPhp('model');    
        
        $this->includeFile('view', ['dataProduct' => $dataProduct]);
        
    }

    private function connectPhp($folder) {

        if ($this->includeFile($folder)) {

            if (class_exists($this->array_path[0] . '_' . $folder)) {
                $name = $this->array_path[0] . '_' . $folder;
                $class = new $name($this->array_path);
                return $class->index();
            }

        }
    }

    private function includeFile($folder, $data = false) {
        if (file_exists(DOCUMENT_ROOT . '/' . $folder . '/' . $this->array_path[0]. '_' . $folder . '.php')) {

            include DOCUMENT_ROOT . '/' . $folder . '/' . $this->array_path[0]. '_' . $folder . '.php';
            return TRUE;
            
        }
        return FALSE;
        
    }

}
