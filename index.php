<?php


if (strpos($_SERVER['REQUEST_URI'], '.php') !== false) {
    header("Location:http://" . $_SERVER['SERVER_NAME']);
    exit;
}

define('ENVIRONMENT', 'development');

switch (ENVIRONMENT) {
    case 'development':
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        ini_set('display_errors', 'on');
        break;

    case 'testing':
    case 'production':
        error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_STRICT ^ E_WARNING);
        ini_set('display_errors', 'off');
        break;

    default:
        header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
        exit('The application environment is not set correctly.');
}


define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('SERVER_NAME', $_SERVER['SERVER_NAME']);


require_once DOCUMENT_ROOT.'/system/router.php';
new Router();